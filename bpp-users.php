<?php 
class productUsers {
	public function __construct() {
		# User roles & capabilities
		## Add capabilites to existing user roles
		add_action('after_setup_theme', array( &$this, 'add_capabilities') );
	} 

   	## Add certain admin roles to editor
   	public function add_capabilities() {
		$roles = new WP_Roles();
		$roles->add_cap('editor','list_users');
		$roles->add_cap('editor','edit_users');
		$roles->add_cap('editor','create_users');
		$roles->add_cap('editor','delete_users');
		$roles->add_cap('editor','edit_theme_options');
   	}

	## Add & Remove contact information fields
	public function ps_dm_user_contactmethods($user_contactmethods){
		$user_contactmethods['phone'] = 'Telefoon';
		unset($user_contactmethods['yim']);
		unset($user_contactmethods['aim']);
		unset($user_contactmethods['jabber']);
		$user_contactmethods['twitter'] = 'Twitter';
		$user_contactmethods['facebook'] = 'Facebook';
		$user_contactmethods['linkedin'] = 'Linkedin';
		$user_contactmethods['user_title'] = 'Website Name';
		$user_contactmethods['functie'] = 'Functie';

		//$user_contactmethods['gplus'] = 'Google Plus';
		return $user_contactmethods;
	}
	### Use this code to embed in template:
	// echo get_user_meta(1, 'twitter', true);
}
new productUsers();