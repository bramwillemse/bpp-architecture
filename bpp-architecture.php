<?php /*
	Plugin Name: Best Product Page - Architecture
	Plugin URI: http://www.bestproductpage.com
	Description: Architecture setup for Best Product Page websites
	Version: 1.0
	Author: Bram Willemse
	Author URI: http://www.bramwillemse.nl
	Tested up to: 4.0
*/

/* 	==============================================================
   	Load Custom Post Types
   	============================================================== */  

	include_once('bpp-post_types.php');
	include_once('bpp-ui.php'); 

/* 	==============================================================
   	Advanced Custom Fields functions
   	============================================================== */  

	include_once('bpp-acf.php'); // Load ACF fields as functions
	// define( 'ACF_LITE', true ); // Remove ACF interface completely
?>