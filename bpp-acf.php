<?php 
/* 	=============================================================================
   	Advanced Custom Fields (plugin)
   	========================================================================== */  

	# Set another title for Options page
	if( function_exists('acf_add_options_page_title') ) {
		acf_set_options_page_title( 'Opties' );
	}
	/**
	* Create options page(s) that sits under the General options menu
	*/
	if( function_exists('acf_add_options_sub_page') )
	{
	    acf_add_options_sub_page(array(
	        'title' => 'Company details',
	        'parent' => 'options-general.php',
	        'capability' => 'manage_options'
	    ));
	}


/* 	=============================================================================
   	Register fields (exports)
   	========================================================================== */  



?>