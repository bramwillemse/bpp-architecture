<?php 
class productPostTypes {

	/**
	 * Constructor, uses hooks to integrate functionalities into WordPress
	 */
	public function __construct() {
		add_action( 'init', array( &$this, 'post_type_features' ), 0 ); // Add post type 'services'
		add_action( 'init', array( &$this, 'post_type_slides' ), 0 ); // Add post type 'customers'
		add_action( 'init', array( &$this, 'post_type_affiliates' ), 0 ); // Add post type 'customers'
		add_action( 'init', array( &$this, 'post_type_videos' ), 0 ); // Add post type 'customers'

		add_action( 'admin_head', array( &$this, 'post_type_icons') ); // set icons for post types
		add_filter( 'request', array( &$this, 'my_custom_archive_order'), 0 ); // Fix archive ordering for post types
		// add_action( 'nav_menu_css_class', array( &$this, 'add_current_nav_class'), 10, 2 );
		add_filter('nav_menu_css_class', array( &$this, 'theme_current_type_nav_class'), 1, 2);
		
	}


	// Register Custom Post Type "Feature"
	public function post_type_features() {
		$labels = array(
			'name'                => _x( 'Features', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Feature', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Features', 'text_domain' ),
			'parent_item_colon'   => __( 'Main feature', 'text_domain' ),
			'all_items'           => __( 'All features', 'text_domain' ),
			'view_item'           => __( 'View feature', 'text_domain' ),
			'add_new_item'        => __( 'Add new feature', 'text_domain' ),
			'add_new'             => __( 'Add new feature', 'text_domain' ),
			'edit_item'           => __( 'Edit feature', 'text_domain' ),
			'update_item'         => __( 'Update feature', 'text_domain' ),
			'search_items'        => __( 'Search features', 'text_domain' ),
			'not_found'           => __( 'No feature found', 'text_domain' ),
			'not_found_in_trash'  => __( 'No feature found in trash', 'text_domain' ),
		);

		$args = array(
			'label'               => __( 'feature', 'text_domain' ),
			'description'         => __( 'features-overzicht', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'revisions' ),
			// 'taxonomies'          => false,
			'hierarchical'        => true,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => 'page',
		);

		register_post_type( 'feature', $args );
	}


	// Register Custom Post Type "Slide"
	public function post_type_slides() {
		$labels = array(
			'name'                => _x( 'Slides', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Slide', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Slides', 'text_domain' ),
			'parent_item_colon'   => __( 'Main slide', 'text_domain' ),
			'all_items'           => __( 'All slides', 'text_domain' ),
			'view_item'           => __( 'View slide', 'text_domain' ),
			'add_new_item'        => __( 'Add new slide', 'text_domain' ),
			'add_new'             => __( 'Add new slide', 'text_domain' ),
			'edit_item'           => __( 'Edit slide', 'text_domain' ),
			'update_item'         => __( 'Update slide', 'text_domain' ),
			'search_items'        => __( 'Search slides', 'text_domain' ),
			'not_found'           => __( 'No slides found', 'text_domain' ),
			'not_found_in_trash'  => __( 'No slides found in trash', 'text_domain' ),
		);

		$args = array(
			'label'               => __( 'slide', 'text_domain' ),
			'description'         => __( 'slides-overzicht', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'revisions' ),
			// 'taxonomies'          => false,
			'hierarchical'        => true,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 6,
			'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => 'page',
		);

		register_post_type( 'slide', $args );
	}


	// Register Custom Post Type "App stores"
	public function post_type_affiliates() {
		$labels = array(
			'name'                => _x( 'Affiliates', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Affiliate', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Affiliates', 'text_domain' ),
			'parent_item_colon'   => __( 'Main Affiliate', 'text_domain' ),
			'all_items'           => __( 'All Affiliates', 'text_domain' ),
			'view_item'           => __( 'View Affiliate', 'text_domain' ),
			'add_new_item'        => __( 'Add new Affiliate', 'text_domain' ),
			'add_new'             => __( 'Add new Affiliate', 'text_domain' ),
			'edit_item'           => __( 'Edit Affiliate', 'text_domain' ),
			'update_item'         => __( 'Update Affiliate', 'text_domain' ),
			'search_items'        => __( 'Search Affiliates', 'text_domain' ),
			'not_found'           => __( 'No Affiliates found', 'text_domain' ),
			'not_found_in_trash'  => __( 'No Affiliates found in trash', 'text_domain' ),
		);

		$args = array(
			'label'               => __( 'affiliate', 'text_domain' ),
			'description'         => __( 'affiliates-overzicht', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'revisions' ),
			// 'taxonomies'          => false,
			'hierarchical'        => true,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 7,
			'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => 'page',
		);

		register_post_type( 'affiliate', $args );
	}


	// Register Custom Post Type "Video"
	public function post_type_videos() {
		$labels = array(
			'name'                => _x( 'Videos', 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( 'Videos', 'text_domain' ),
			'parent_item_colon'   => __( 'Main Video', 'text_domain' ),
			'all_items'           => __( 'All Videos', 'text_domain' ),
			'view_item'           => __( 'View Video', 'text_domain' ),
			'add_new_item'        => __( 'Add new video', 'text_domain' ),
			'add_new'             => __( 'Add new video', 'text_domain' ),
			'edit_item'           => __( 'Edit Video', 'text_domain' ),
			'update_item'         => __( 'Update Video', 'text_domain' ),
			'search_items'        => __( 'Search Videos', 'text_domain' ),
			'not_found'           => __( 'No Videos found', 'text_domain' ),
			'not_found_in_trash'  => __( 'No Videos found in trash', 'text_domain' ),
		);

		$args = array(
			'label'               => __( 'video', 'text_domain' ),
			'description'         => __( 'videos-overzicht', 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'revisions' ),
			// 'taxonomies'          => false,
			'hierarchical'        => true,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 8,
			'menu_icon'           => '',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => 'page',
		);

		register_post_type( 'video', $args );
	}

	// Add icons to Post types
	public function post_type_icons() { ?>
	    <style type="text/css" media="screen">
			#adminmenu .menu-icon-page div.wp-menu-image:before {
				content: "\f116";
			}

			#adminmenu .menu-icon-media div.wp-menu-image:before {
				content: "\f116";
			}

			#adminmenu .menu-icon-affiliate div.wp-menu-image:before {
				content: "\f338";
			}

			#adminmenu .menu-icon-feature div.wp-menu-image:before {
				content: "\f323";
			}

			#adminmenu .menu-icon-slide div.wp-menu-image:before {
				content: "\f496";
			}

			#adminmenu .menu-icon-video div.wp-menu-image:before {
				content: "\f236";
			}

	    </style><?php 
	} 
	// Use custom sort order post types
	public function my_custom_archive_order( $vars ) {
		if ( !is_admin() && isset($vars['post_type']) && is_post_type_hierarchical($vars['post_type']) ) {
			$vars['orderby'] = 'menu_order';
			$vars['order'] = 'ASC';
		}
		return $vars;
	}

	// Highlight post type in nav menu
	public function add_current_nav_class($classes, $item) {
		
		// Getting the current post details
		global $post;
		
		// Getting the post type of the current post
		$current_post_type = get_post_type_object(get_post_type($post->ID));
		$current_post_type_slug = $current_post_type->rewrite['slug'];
			
		// Getting the URL of the menu item
		$menu_slug = strtolower(trim($item->url));
		
		// If the menu item URL contains the current post types slug add the current-menu-item class
		if (strpos($menu_slug,$current_post_type_slug) !== false) {
		
		   $classes[] = 'current-menu-item';
		
		}
		
		// Return the corrected set of classes to be added to the menu item
		return $classes;
	
	}	

	public function theme_current_type_nav_class($css_class, $item) {
	    static $custom_post_types, $post_type, $filter_func;

	    if (empty($custom_post_types))
	        $custom_post_types = get_post_types(array('_builtin' => false));

	    if (empty($post_type))
	        $post_type = get_post_type();

	    if ('page' == $item->object && in_array($post_type, $custom_post_types)) {
	        if (empty($filter_func))
	            $filter_func = create_function('$el', 'return ($el != "current_page_parent");');

	        $css_class = array_filter($css_class, $filter_func);

	        $template = get_page_template_slug($item->object_id);
	        if (!empty($template) && preg_match("/^page(-[^-]+)*-$post_type/", $template) === 1)
	            array_push($css_class, 'current_page_parent');

	    }

	    return $css_class;
	}



}
new productPostTypes;

?>